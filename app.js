const express = require('express');
const app = express();
const exphbs = require('express-handlebars');
const port = 3000;
const cookieParser = require('cookie-parser')
const session = require('express-session')
var HTMLParser = require('node-html-parser')






app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true,
}))

app.use(cookieParser())
app.use(express.urlencoded({ extended: true }));
app.engine('hbs', exphbs({
    defaultLayout: 'main.hbs'
}))
app.set('view engine', 'hbs');
app.use(express.static('./public/'));

app.get('/index.html', (req, res) => {
    res.render('home');
})
app.use('/', require('./routers/register.router'));
//app.use('/admin/config', require('./routers/conference.router'));

app.get('/t', (req, res) => {
    
    
    // var root = HTMLParser.parse('<ul id="list"><li>Hello World</li></ul>');
    // console.log(root.firstChild.structure);
    // console.log(root);
    res.render('t')

});
app.use(function (req, res) {
    res.render('404', { layout: false });
})

app.listen(port, () => {
    console.log(`Server start port at http://localhost:${port}`);
})
