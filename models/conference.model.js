const db = require('../utils/db');

const TABLE_CONFERENCE = 'conference'
module.exports = {
    all: function () {
        return db.load(`select * from ${TABLE_CONFERENCE}`);
    },
    insertCon: function (entity) {
        return db.insert(`insert into  ${TABLE_CONFERENCE} set ?`, entity);
    }
    ,
    single: function (id) {
        return db.load(`select * from ${TABLE_CONFERENCE} where  c_ID = ${id}`);
    }
    ,
    patch: function (entity) {
        const condition = {
            c_ID: entity.c_ID
        }
        delete entity.c_ID;
        return db.update(`update ${TABLE_CONFERENCE} set ? where ?`, entity, condition);
    },
    del: function (id) {
        const condition = {
            c_ID: id
        }
        return db.del(`delete from ${TABLE_CONFERENCE} where ?`, condition);
    },
    Con_NotAppear: function () {
        return db.load(`
        select c_ID ,c_Status,c_Name
       from conference  where c_Status=0`)
    }

}